<?php

$id = 'contact'; // body id attribute

// Class names to be applied to body element
$classes = Array(
    'collapsedHeader'
);

$ns = 'contact'; // Javascript namespace

require_once('includes/header.php');

?>
        <div class="wrapper">
            <div class="hWrapper">
                <h2>Contact Federal Protection</h2>
                <img src="images/placeholders/contact.jpg" width="2000" height="174" alt="" />
            </div>
            <form action="" method="post" class="left-col">
                <h2>Send Us A Message</h2>
                <p>Intro to the contact form goes here. Ut in nulla enim. Phasellus molestie magna non est bibendum.</p>
                <label>
                    Your name
                    <input type="text" name="#" value="" />
                </label>
                <label class="error">
                    Company name
                    <input type="text" name="#" value="" />
                </label>
                <label>
                    How can we help you?
                    <input type="text" name="#" value="" />
                </label>
                <label>
                    Phone number <em>Required</em>
                    <input type="text" name="#" value="" />
                </label>
                <label>
                    Email address <em>Required</em>
                    <input type="text" name="#" value="" />
                </label>
                <label>
                    Comments
                    <textarea name="#"></textarea>
                </label>
                <button type="submit">Send your message now</button>
            </form>
            <section class="right-col">
                <div class="frame pull-up-20">
                    <span class="border">
                        <img src="images/placeholders/contact_map.jpg" width="470" height="305" alt="" />
                    </span>
                </div>

                <ul class="locations">

                    <li class="home">
                        <h4>Home Office Location</h4>
                        <span class="address">2500 N Airport Commerce, Springfield, MO 65803</span>
                        <span class="email">Email@address.com</span>
                        <span class="tel">800.299.5400</span>
                        <a href="#">See On Map</a>
                    </li>

                    <li>
                        <h5>Home Office Location</h5>
                        <span class="email">Email@address.com</span>
                        <span clas="tel">800.299.5400</span>
                        <a href="#">See On Map</a>
                    </li>

                    <li>
                        <h5>Home Office Location</h5>
                        <span class="email">Email@address.com</span>
                        <span clas="tel">800.299.5400</span>
                        <a href="#">See On Map</a>
                    </li>

                    <li>
                        <h5>Home Office Location</h5>
                        <span class="email">Email@address.com</span>
                        <span clas="tel">800.299.5400</span>
                        <a href="#">See On Map</a>
                    </li>

                    <li>
                        <h5>Home Office Location</h5>
                        <span class="email">Email@address.com</span>
                        <span clas="tel">800.299.5400</span>
                        <a href="#">See On Map</a>
                    </li>

                    <li>
                        <h5>Home Office Location</h5>
                        <span class="email">Email@address.com</span>
                        <span clas="tel">800.299.5400</span>
                        <a href="#">See On Map</a>
                    </li>

                    <li>
                        <h5>Home Office Location</h5>
                        <span class="email">Email@address.com</span>
                        <span clas="tel">800.299.5400</span>
                        <a href="#">See On Map</a>
                    </li>

                    <li>
                        <h5>Home Office Location</h5>
                        <span class="email">Email@address.com</span>
                        <span clas="tel">800.299.5400</span>
                        <a href="#">See On Map</a>
                    </li>

                    <li>
                        <h5>Home Office Location</h5>
                        <span class="email">Email@address.com</span>
                        <span clas="tel">800.299.5400</span>
                        <a href="#">See On Map</a>
                    </li>

                    <li class="viewAll">
                        <a href="#">View All Locations</a>
                    </li>
                </ul>
            </section>
        </div>
<?php require_once('includes/footer.php'); ?>
