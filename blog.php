<?php

$id = 'blog'; // body id attribute

// Class names to be applied to body element
$classes = Array(
    'collapsedHeader'
);

$ns = 'about'; // Javascript namespace

require_once('includes/header.php');

?>
        <div class="wrapper">
            <div class="hWrapper">
                <h2>Federal Protection Blog</h2>
                <img src="images/placeholders/blog_landing.jpg" width="2000" height="174" alt="" />
            </div>
            <nav class="left-col">
                <ul>
                    <li>Categories
                        <ul>
                            <li><a href="#" title="Overview">Overview</a></li>
                            <li><a href="#" title="Views &amp; Mission">Views &amp; Mission</a></li>
                            <li><a href="#" title="History &amp; Heritage">History &amp; Heritage</a></li>
                            <li><a href="#" title="Federal Response Center">Federal Response Center</a></li>
                            <li><a href="#" title="Our Location">Our Locations</a></li>
                        </ul>
                    </li>
                    <li>Archives
                        <ul>
                            <li><a href="#" title="Overview">Overview</a></li>
                            <li><a href="#" title="Views &amp; Mission">Views &amp; Mission</a></li>
                            <li><a href="#" title="History &amp; Heritage">History &amp; Heritage</a></li>
                            <li><a href="#" title="Federal Response Center">Federal Response Center</a></li>
                            <li><a href="#" title="Our Location">Our Locations</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
            <section class="right-col interior">

                <article>

                    <h3><a href="#">Headline 2 Looks Like This</a></h3>

                    <dl>

                        <dt>Date</dt>

                        <dd>March 3, 2013</dd>

                        <dt>Category</dt>

                        <dd>In condimentum</dd>
                        
                    </dl>

                    <p>Nulla at nulla justo, eget luctus tortor. Nulla facilisi. Duis aliquet egestas purus in blandit. Curabitur vulputate, ligula lacinia scelerisque tempor, lacus lacus ornare ante, ac egestas est urna sit amet arcu. Class aptent.Praesent id metus massa, ut blandit odio. Proin quis tortor orci. Etiam at risus et justo dignissim congue. Donec congue lacinia dui, a porttitor lectus condimentum laoreet. Nunc eu ullamcorper orci. Quisque eget odio ac lectus vestibulum faucibus eget in metus. In pellentesque faucibus vestibulum. Nulla at nulla justo, eget luctus tortor. Nulla facilisi. Duis aliquet egestas purus in blandit. Curabitur vulputate, ligula lacinia scelerisque tempor, lacus lacus ornare ante, ac egestas est urna sit amet arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed molestie augue sit amet leo consequat posuere. Vestibulum ante ipsum primis in faucibus. Duis aliquet egestas purus in blandit. Curabitur vulputate, ligula lacinia scelerisque tempor.</p>

                </article>

                <article>

                    <h3><a href="#">Headline 2 Looks Like This</a></h3>

                    <dl>

                        <dt>Date</dt>

                        <dd>March 3, 2013</dd>

                        <dt>Category</dt>

                        <dd>In condimentum</dd>
                        
                    </dl>

                    <p>Nulla at nulla justo, eget luctus tortor. Nulla facilisi. Duis aliquet egestas purus in blandit. Curabitur vulputate, ligula lacinia scelerisque tempor, lacus lacus ornare ante, ac egestas est urna sit amet arcu. Class aptent.Praesent id metus massa, ut blandit odio. Proin quis tortor orci. Etiam at risus et justo dignissim congue. Donec congue lacinia dui, a porttitor lectus condimentum laoreet. Nunc eu ullamcorper orci. Quisque eget odio ac lectus vestibulum faucibus eget in metus. In pellentesque faucibus vestibulum. Nulla at nulla justo, eget luctus tortor. Nulla facilisi. Duis aliquet egestas purus in blandit. Curabitur vulputate, ligula lacinia scelerisque tempor, lacus lacus ornare ante, ac egestas est urna sit amet arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed molestie augue sit amet leo consequat posuere. Vestibulum ante ipsum primis in faucibus. Duis aliquet egestas purus in blandit. Curabitur vulputate, ligula lacinia scelerisque tempor.</p>
                    <p>Nulla at nulla justo, eget luctus tortor. Nulla facilisi. Duis aliquet egestas purus in blandit. Curabitur vulputate, ligula lacinia scelerisque tempor, lacus lacus ornare ante, ac egestas est urna sit amet arcu. Class aptent.Praesent id metus massa, ut blandit odio. Proin quis tortor orci. Etiam at risus et justo dignissim congue. Donec congue lacinia dui, a porttitor lectus condimentum laoreet. Nunc eu ullamcorper orci. Quisque eget odio ac lectus vestibulum faucibus eget in metus. In pellentesque faucibus vestibulum. Nulla at nulla justo, eget luctus tortor. Nulla facilisi. Duis aliquet egestas purus in blandit. Curabitur vulputate, ligula lacinia scelerisque tempor, lacus lacus ornare ante, ac egestas est urna sit amet arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed molestie augue sit amet leo consequat posuere. Vestibulum ante ipsum primis in faucibus. Duis aliquet egestas purus in blandit. Curabitur vulputate, ligula lacinia scelerisque tempor.</p>

                </article>

            </section>
        </div>
<?php require_once('includes/footer.php'); ?>
