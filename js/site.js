var Site = Site || {};
Site.pages = Site.pages || {};

$(function(e) {
    var whereAmI = $('body').attr('data-page');
    try {
        Site.pages[whereAmI].ready();
    } catch(error) {
        //console.error('Unable to locate that page:', error);
    }

    Sticky.init();
});

Site.pages.home = Site.pages.home || {};

Site.pages.home = {
    ready: function() {
        var carousel = $('.carousel').jcarousel({
                    'wrap': 'both'
                }),
            itemCount = carousel.find('.items li').length;

        $('.carousel').on('itemtargetin.jcarousel', 'li', function(event, carousel) {
            //  Clear previously set timer
            window.clearInterval(timer);

            // Set new timer
            setTimer();
        });
        
        if (itemCount > 1) {
                // New nav for the carousel
            var nav = $('<ul />').addClass('nav'),

                // Array to store dots for each carousel item
                dots = [],

                // Holds the timer
                timer = null,

                // Timer
                setTimer = function() {
                    timer = window.setInterval(function() {
                        carousel.jcarousel('scroll', '+=1');
                    }, 5000);
                },

                // Previous arrow link
                prev = $('<li />')
                    .addClass('prev')
                    .append($('<a />')
                        .text('Previous')
                        .attr('href', '#') 
                        .bind('click', function(e) {
                            e.preventDefault();
                            carousel.jcarousel('scroll', '-=1');
                        })),

                // Next arrow link
                next = $('<li />')
                    .addClass('next')
                    .append($('<a />')
                        .text('Next')
                        .attr('href', '#') 
                        .bind('click', function(e) {
                            e.preventDefault();
                            carousel.jcarousel('scroll', '+=1');
                        }));

            // Add previous link
            nav.append(prev);

            // Indivdual items
            carousel.find('.items li').each(function(i, el) {

                // Build new anchor
                var anchor = $('<a />')
                        .text(i + 1)
                        .attr('href', '#') 
                        .bind('click', function(e) {
                                e.preventDefault();
                                carousel.jcarousel('scroll', i);
                            }),

                    li = $('<li />').append(anchor);

                    // If we are on the first item, set the first dot to be
                    // selected.
                    if (i === 0) li.addClass('sel');

                    // Bind event to carousel item; event fires when this item
                    // becomes target of carousel.
                    $(el).bind('itemtargetin.jcarousel', function(event, carousel) {

                        // Loop over dots and remove 'sel' class
                        $.each(dots, function(i, v) {
                            v.removeClass('sel');
                        });

                        // Add 'sel' class to the list item that is related to the
                        // targetted item of the carousel
                        li.addClass('sel');
                     });

                // Add new anchor to our dots array
                dots.push(li);

                // Append new list item to nave
                nav.append(li);
            });

            // Add next link
            nav.append(next);

            // Attach nav to carousel
            carousel.append(nav);

            // Default timer
            setTimer();
        }
   }
}

Site.pages.about = {
    ready: function() {
        var carousel = $('.carousel').jcarousel({
                    'wrap': 'circular',
                    'animation': 500
                }),
            itemCount = carousel.find('.items li').length;

        if (itemCount > 1) {
            window.setInterval(function() {
                carousel.jcarousel('scroll', '+=1');
            }, 5000);
        }
   }
}

Sticky = {
    breakDown: 42,

    breakUp: 42,

    placeholder: null,

    init: function() {

        Sticky.window = $(window);
        Sticky.header = $('header');
        Sticky.headerLogo = Sticky.header.find('h1 a img');

        Sticky.stick();
    },
    
    stick: function() {
        var _window = Sticky.window;

        _window.on('scroll.stick', function() {

            if (_window.scrollTop() > Sticky.breakDown) {
                Sticky.header.addClass('collapsed')
            
                // We need a placeholder element that takes the place of the
                // header in the flow of the page.  If we don't then the page
                // content will jump up when the header becomes sticky.
                // If there is no placeholder in the markup already, create one.
                if (Sticky.placeholder === null) {
                    Sticky.placeholder = $('<div id="placeholder" />');
                    Sticky.placeholder.insertAfter(Sticky.header);
                }else{
                    // If there is already a placeholder, show it.
                    Sticky.placeholder.show();
                }

                _window.off('scroll.stick');
                Sticky.unstick();
            }

        });
    },

    unstick: function() {
        var _window = Sticky.window;

        _window.on('scroll.unstick', function() {

            if (_window.scrollTop() < Sticky.breakUp) {
                Sticky.header.removeClass('collapsed');

                // Hide placeholder, if one exists.
                if (Sticky.placeholder !== null) {
                    Sticky.placeholder.hide();
                }

                _window.off('scroll.unstick');
                Sticky.stick();
            }

        });
    },
}
