<?php

$id = 'about'; // body id attribute

// Class names to be applied to body element
$classes = Array(
    'collapsedHeader'
);

$ns = 'aboutDetail'; // Javascript namespace

require_once('includes/header.php');

?>
        <div class="wrapper">
            <div class="hWrapper">
                <h2>Values &amp; Mission</h2>
                <img src="images/placeholders/about_us_detail.jpg" width="2000" height="174" alt="" />
            </div>
            <nav class="left-col">
                <ul>
                    <li><a href="about.php" title="Overview">Overview</a></li>
                    <li class="sel"><a href="about_detail.php" title="Views &amp; Mission">Views &amp; Mission</a></li>
                    <li><a href="about_detail.php" title="History &amp; Heritage">History &amp; Heritage</a></li>
                    <li><a href="about_detail.php" title="Federal Response Center">Federal Response Center</a></li>
                    <li><a href="about_detail.php" title="Our Location">Our Locations</a></li>
                </ul>
            </nav>
            <section class="right-col interior">
                <h3>Headline 2 Looks Like This</h3>

                <p>Nulla at nulla justo, eget luctus tortor. Nulla facilisi. Duis aliquet egestas purus in blandit. Curabitur vulputate, ligula lacinia scelerisque tempor, lacus lacus ornare ante, ac egestas est urna sit amet arcu. Class aptent.Praesent id metus massa, ut blandit odio. Proin quis tortor orci. Etiam at risus et justo dignissim congue. Donec congue lacinia dui, a porttitor lectus condimentum laoreet. Nunc eu ullamcorper orci. Quisque eget odio ac lectus vestibulum faucibus eget in metus. In pellentesque faucibus vestibulum. Nulla at nulla justo, eget luctus tortor. Nulla facilisi. Duis aliquet egestas purus in blandit. Curabitur vulputate, ligula lacinia scelerisque tempor, lacus lacus ornare ante, ac egestas est urna sit amet arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed molestie augue sit amet leo consequat posuere. Vestibulum ante ipsum primis in faucibus. Duis aliquet egestas purus in blandit. Curabitur vulputate, ligula lacinia scelerisque tempor.</p>

                <div class="frame">
                    <span class="border">
                        <img src="images/placeholders/about_us_cta.jpg" width="630" height="350" alt="" />
                    </span>
                    <p>Photo caption goes here. Duis aliquet egestas purus in blandit. Curabitur vulputate.</p>
                </div>

                <p>Nulla at nulla justo, eget luctus tortor. Nulla facilisi. Duis aliquet egestas purus in blandit. Curabitur vulputate, ligula lacinia scelerisque tempor, lacus lacus ornare ante, ac egestas est urna sit amet arcu. Class aptent.Praesent id metus massa, ut blandit odio. Proin quis tortor orci. Etiam at risus et justo dignissim congue. Donec congue lacinia dui, a porttitor lectus condimentum laoreet. Nunc eu ullamcorper orci. Quisque eget odio ac lectus vestibulum faucibus eget in metus. In pellentesque faucibus vestibulum. Nulla at nulla justo, eget luctus tortor. Nulla facilisi. Duis aliquet egestas purus in blandit. Curabitur vulputate, ligula lacinia scelerisque tempor, lacus lacus ornare ante, ac egestas est urna sit amet arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed molestie augue sit amet leo consequat posuere. Vestibulum ante ipsum primis in faucibus. Duis aliquet egestas purus in blandit. Curabitur vulputate, ligula lacinia scelerisque tempor.</p>

                <h4>Header 3 Looks Like This</h4>

                <p>Nulla at nulla justo, eget luctus tortor. Nulla facilisi. Duis aliquet egestas purus in blandit. Curabitur vulputate, ligula lacinia scelerisque tempor, lacus lacus ornare ante, ac egestas est urna sit amet arcu. Class aptent.Praesent id metus massa, ut blandit odio. Proin quis tortor orci. Etiam at risus et justo dignissim congue. Donec congue lacinia dui, a porttitor lectus condimentum laoreet. Nunc eu ullamcorper orci. Quisque eget odio ac lectus vestibulum faucibus eget in metus. In pellentesque faucibus vestibulum. Nulla at nulla justo, eget luctus tortor. Nulla facilisi. Duis aliquet egestas purus in blandit. Curabitur vulputate, ligula lacinia scelerisque tempor, lacus lacus ornare ante, ac egestas est urna sit amet arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed molestie augue sit amet leo consequat posuere. Vestibulum ante ipsum primis in faucibus. Duis aliquet egestas purus in blandit. Curabitur vulputate, ligula lacinia scelerisque tempor.</p>

                <div class="frameGroup">

                    <div class="frame">
                        <span class="border">
                            <img src="images/placeholders/about_us_frame_group.jpg" width="290" height="290" alt="" />
                        </span>
                        <p>Photo caption goes here.</p>
                    </div>

                    <div class="frame">
                        <span class="border">
                            <img src="images/placeholders/about_us_frame_group.jpg" width="290" height="290" alt="" />
                        </span>
                        <p>Photo caption goes here.</p>
                    </div>

                </div>

            </section>
        </div>
<?php require_once('includes/footer.php'); ?>
