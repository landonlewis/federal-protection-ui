        <footer>
            <div class="container">
                <nav>
                    <ul>
                        <li class="financial">
                            <h3>Financial</h3>
                            <ul>
                                <li><a href="#">Federal Conect</a></li>
                                <li><a href="#">Alarm Systems</a></li>
                                <li><a href="#">Access Control</a></li>
                                <li><a href="#">Video Surveillance</a></li>
                                <li><a href="#">Res Link 5</a></li>
                            </ul>
                        </li>
                        <li class="commercial">
                            <h3>Commercial</h3>
                            <ul>
                                <li><a href="#">Federal Conect</a></li>
                                <li><a href="#">Alarm Systems</a></li>
                                <li><a href="#">Access Control</a></li>
                                <li><a href="#">Video Surveillance</a></li>
                                <li><a href="#">Res Link 5</a></li>
                                <li><a href="#">Res Link 6</a></li>
                            </ul>
                        </li>
                        <li class="other">
                            <h3>Other</h3>
                            <ul>
                                <li><a href="#">Residential</a></li>
                                <li><a href="about.php">About Us</a></li>
                                <li><a href="#">Contact Us</a></li>
                                <li><a href="#">Careers</a></li>
                                <li><a href="#">Login</a></li>
                                <li><a href="#">Blog</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>

                <div>
                    <img src="images/layout/footer_logo.png" width="220" height="111" alt="" />
                    <span><a href="tel:1-800-299-5400">800.299.5400</a></span>
                    <span><a href="mailto:info@federalprotection.com">info@federalprotection.com</a></span>
                    <span>2500 N Airport Commerce, <br />Springfield, MO 65803</span>
                </div>

                <p>&copy; 2013 Federal Protection, all rights reserved.</p>
            </div>
        </footer>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.9.1.min.js"><\/script>')</script>

        <!--[if (gte IE 6)&(lte IE 8)]>
          <script type="text/javascript" src="js/selectivizr-min.js"></script>
        <![endif]-->

        <script src="js/libs.js"></script>
        <script src="js/site.js"></script>
    </body>
</html>
