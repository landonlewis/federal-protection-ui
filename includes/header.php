<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <link rel="stylesheet" href="css/global.css">
        <link rel="stylesheet" href="css/site.css">

        <!--[if lt IE 9]>
            <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
            <script>window.html5 || document.write('<script src="js/vendor/html5shiv.js"><\/script>')</script>
        <![endif]-->

        <script type="text/javascript" src="//use.typekit.net/oov2ych.js"></script>
        <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
    </head>
    <body<?php echo (isset($id)) ? ' id="' . $id . '"' : ''; echo (isset($ns)) ? ' data-page="' . $ns. '"' : ''; echo (isset($classes) && count($classes) > 0) ? ' class="' . implode(' ', $classes) . '"' : ''; ?>>
        <!--[if lt IE 8]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <header>
            <div class="container">
                <h1><a href="index.php"><img src="images/layout/header_logo.png" width="343" height="62" alt="Federal Protection" /></a></h1>

                <nav>
                    <ul>
                        <li<?php echo (isset($id) && $id == 'about') ? ' class="sel"' : ''; ?>><a href="about.php" class="about">About Us</a></li>
                        <li<?php echo (isset($id) && $id == 'financial') ? ' class="sel"' : ''; ?>><a href="#" class="financial">Financial</a></li>
                        <li><a href="#" class="commercial">Commercial</a></li>
                        <li><a href="#">Residential</a></li>
                        <li<?php echo (isset($id) && $id == 'contact') ? ' class="sel"' : ''; ?>><a href="contact.php">Contact Us</a></li>
                    </ul>
                </nav>

                <div>
                    <span>Call Toll Free <a href="tel:1-800-299-5400">800.299.5400</a></span>
                    <nav>
                        <ul>
                            <li><a href="#">Careers</a></li>
                            <li><a href="#">Login</a></li>
                            <li><a href="blog.php">Blog</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </header>
