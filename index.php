<?php

$id = 'landing'; // body id attribute
$ns = 'home'; // Javascript namespace
require_once('includes/header.php');

?>
        <section>
            <div class="wrapper">
                <div class="hWrapper">
                    <div class="carousel">
                        <ul class="items">
                            <li>
                                <h2>This is a heading</h2>
                                <p>This is a line that will go under the header and will talk about whatever it is that we’re talking about.</p>
                                <a href="#" class="btn white">Contact Federal Protection</a>
                                <img src="images/placeholders/landing_big_bg" width="2000" height="700" alt="" />
                            </li>
                            <li>
                                <h2>This is a heading 2</h2>
                                <p>This is a line that will go under the header and will talk about whatever it is that we’re talking about.</p>
                                <a href="#" class="btn white">Contact Federal Protection</a>
                                <img src="images/placeholders/landing_big_bg" width="2000" height="700" alt="" />
                            </li>
                            <li>
                                <h2>This is a heading 3</h2>
                                <p>This is a line that will go under the header and will talk about whatever it is that we’re talking about.</p>
                                <a href="#" class="btn white">Contact Federal Protection</a>
                                <img src="images/placeholders/landing_big_bg" width="2000" height="700" alt="" />
                            </li>
                            <li>
                                <h2>This is a heading 4</h2>
                                <p>This is a line that will go under the header and will talk about whatever it is that we’re talking about.</p>
                                <a href="#" class="btn white">Contact Federal Protection</a>
                                <img src="images/placeholders/landing_big_bg" width="2000" height="700" alt="" />
                            </li>
                        </ul>
                    </div>
                </div>

                <ul class="cta">
                    <li class="frame one-third">
                        <span class="border">
                            <img src="images/placeholders/cta1.jpg" width="290" height="180" alt="" />
                        </span>
                        <h3>Call to action goes here</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in dui mauris. Vivamus hendrerit arcu sed erat molestie vehicula. Sed auctor neque eu tellus rhoncus ut eleifend nibh port.</p>
                        <a href="#" class="learn-more btn grey">Learn More</a>
                    </li>
                    <li class="frame one-third">
                        <span class="border">
                            <img src="images/placeholders/cta2.jpg" width="290" height="180" alt="" />
                        </span>
                        <h3>Call to action goes here</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in dui mauris. Vivamus hendrerit arcu sed erat molestie vehicula. Sed auctor neque eu tellus rhoncus ut eleifend nibh port.</p>
                        <a href="#" class="learn-more btn grey">Learn More</a>
                    </li>
                    <li class="frame one-third">
                        <span class="border">
                            <img src="images/placeholders/cta3.jpg" width="290" height="180" alt="" />
                        </span>
                        <h3>Call to action goes here</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in dui mauris. Vivamus hendrerit arcu sed erat molestie vehicula. Sed auctor neque eu tellus rhoncus ut eleifend nibh port.</p>
                        <a href="#" class="learn-more btn grey">Learn More</a>
                    </li>
                </ul>
            </div>
        </section>
<?php require_once('includes/footer.php'); ?>
